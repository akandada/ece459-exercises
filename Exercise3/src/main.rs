use std::thread;

static N: i32 = 10;  // number of threads to spawn

// You should modify main() to spawn multiple threads and join() them
fn main() {
    // let mut children = vec![];

    // let mut children : Vec<thread::JoinHandle>;
    let mut children = vec![];

    for i in 0..N{
        children.push( thread::spawn( move || {
            thread::sleep(std::time::Duration::from_secs(2));
            println!("I am thread {}!", i);
            thread::sleep(std::time::Duration::from_secs(2));
        }))
    }

    for child in children{
        child.join();
    }

}

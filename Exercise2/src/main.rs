// You should implement the following function
fn fibonacci_number(n: u32) -> u32 {
    if n <= 1 {
        return 1
    }else{
        // return fibonacci_number(n - 1) + fibonacci_number(n - 2)
        let mut n1 = 1;
        let mut n2 = 1;
        // let mut next_soln = 0;
        for _ in 1..n{
            let next_soln = n1 + n2;
            n2 = n1;
            n1 = next_soln;
        }

        return n1;
    }

}


fn main() {
    println!("{}", fibonacci_number(10));
}

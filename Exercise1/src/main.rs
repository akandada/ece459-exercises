// You should implement the following function:

fn sum_of_multiples(number: i32, multiple1: i32, multiple2: i32) -> i32 
{
    let mut sum = 0;
    for i in 1..number{
        if ( i % multiple1 == 0) || (i % multiple2 == 0){
            sum += i;
        }
    }
    sum
}

fn main() {
    let mut N = 1000; let mut m1=5; let mut m2=3;
    println!("{}", sum_of_multiples(N, m1, m2));
    N=100; m1=7; m2=9;

    println!("{}", sum_of_multiples(N, m1, m2));
}

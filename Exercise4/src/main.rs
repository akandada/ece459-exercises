use std::sync::mpsc;
use std::thread;
use std::time::Duration;


static N : u32 = 10;


fn print_to_receiver(rx : &mpsc::Receiver<String>){
    for received in rx {
        println!("Got: {}", received);
    }
}

// You should modify main() to spawn threads and communicate using channels
fn main() {
    let (tx, rx) = mpsc::channel();

    let mut children = vec![];

    for i in 0..N{
        let tx1 = mpsc::Sender::clone(&tx);
        children.push( thread::spawn( move || {
            loop{
                let msg = format!("Message from thread {}", i);
                tx1.send(msg).unwrap();
                thread::sleep(std::time::Duration::from_secs(1));
            }
        }))
    }

    
    let printer = thread::spawn(move || {
        loop{
            print_to_receiver(&rx);
            thread::sleep(std::time::Duration::from_secs(1));
        }
    });

    for child in children{
        child.join();
    }

    printer.join();



}
